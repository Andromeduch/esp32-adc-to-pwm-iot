#define BLINK_GPIO GPIO_NUM_16// 16 for esp32 with 18650

// ADC_WIDTH_BIT_12
#define NUMBER_OF_SEMPLS 50
extern volatile int read_raw_1;
extern volatile int read_raw_2;

extern volatile int MotorSpeed;
extern volatile int MotorDirection;

extern volatile int fader1_topicValue;
extern volatile int fader2_topicValue;

extern volatile int motorDirection_topicValue;
extern volatile int motorSpeed_topicValue;
// ----|-LED_blindSpot-|------ADC-----|-LED_blindSpot-|---
#define LED_blindSpot 70 // ADC steps
#define ADC_minVal (115 + LED_blindSpot)
#define ADC_maxVal (3190 - LED_blindSpot - ADC_minVal) // = 3075 - 30*2 = 3015
#define PWM_fullDuty 8192

#define WIFI_SSID      "MyHomeNet"
#define WIFI_PASS      "0937624716"

#define TOPIC_FADER1 "/esp32/fader1"
#define TOPIC_FADER2 "/esp32/fader2"

#define TOPIC_MOTOR_SPEED "/esp32/MotorSpeed"
#define TOPIC_MOTOR_DIRECTION "/esp32/MotorDirection"

#define TOPIC_KARNIZ "Curtains/direction"
#define KARNIZ_STOP '0'
#define KARNIZ_LEFT '1'
#define KARNIZ_RIGHT '2'

#define MQTT_CLIENT_ID "MAIN_RC_LIGHT_ESP32"

#define RUN_readADC_task false
#define RUN_a4988_task false
//#define RC_BTN_PRESENT

#define STEP_IO GPIO_NUM_27
#define DIR_IO  GPIO_NUM_14


#define LEDC_HS_CH1_GPIO       (33) // GPIO NUM 33
#define LEDC_HS_CH0_GPIO       (32) // GPIO NUM 32

#define KARNIZ_pin1 GPIO_NUM_15 // RTC_GPIO_13
#define KARNIZ_pin2 GPIO_NUM_2 // RTC_GPIO_12
#define KARNIZ_pin3 GPIO_NUM_4 // RTC_GPIO_10