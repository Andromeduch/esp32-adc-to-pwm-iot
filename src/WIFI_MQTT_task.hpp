#include "global.hpp"


// =======================================================
// WIFI
// Example: https://github.com/espressif/esp-idf/blob/v3.2/examples/protocols/mqtt/tcp/main/app_main.c

#include "nvs_flash.h"
#include "tcpip_adapter.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_wifi.h"
#include "freertos/event_groups.h"
#include "mqtt_client.h"

#include <algorithm>

#include "xtensa/core-macros.h"



static EventGroupHandle_t wifi_event_group;
const static int CONNECTED_BIT = BIT0;

// extern 
volatile int fader1_topicValue = 0;
volatile int fader2_topicValue = 0;
volatile int motorDirection_topicValue = 0;
volatile int motorSpeed_topicValue = 0;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            printf("MQTT_EVENT_CONNECTED\n");

            msg_id = esp_mqtt_client_subscribe(client, TOPIC_FADER1, 0);
            // printf("sent subscribe successful, msg_id=%d\n", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, TOPIC_FADER2, 0);
            // printf("sent subscribe successful, msg_id=%d\n", msg_id);

            msg_id = esp_mqtt_client_subscribe(client, TOPIC_MOTOR_SPEED, 0);
            // printf("sent subscribe successful, msg_id=%d\n", msg_id);
            msg_id = esp_mqtt_client_subscribe(client, TOPIC_MOTOR_DIRECTION, 0);
            // printf("sent subscribe successful, msg_id=%d\n", msg_id);
            
            

            // msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, 1, 0);
            // printf("sent publish successful, msg_id=%d\n", msg_id);

            break;
        case MQTT_EVENT_DISCONNECTED:
            printf("MQTT_EVENT_DISCONNECTED\n");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            printf("MQTT_EVENT_SUBSCRIBED, msg_id=%d\n", event->msg_id);
            // msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
            // printf("sent publish successful, msg_id=%d\n", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            printf("MQTT_EVENT_UNSUBSCRIBED, msg_id=%d\n", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            printf("MQTT_EVENT_PUBLISHED, msg_id=%d\n", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            printf("MQTT_EVENT_DATA:\n");
            printf("\tTOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("\tDATA=%.*s\r\n", event->data_len, event->data);

            {
                const int buffSize = 64;
                static char topic[buffSize] = "";
                // topic[buffSize-1] = '\0';
                memset(topic, 0, buffSize);
                strncpy(topic, event->topic, std::min(buffSize-1, event->topic_len));

               // const int buffSize = 128;
                static char data[buffSize] = "";
                //data[buffSize-1] = '\0';
                memset(data, 0, buffSize);
                strncpy(data, event->data, std::min(buffSize-1, event->data_len));
                
                if(!strcmp(topic, TOPIC_FADER1))
                {
                    fader1_topicValue = atoi(data);
                    printf("\tGet fader1 = %i\n", fader1_topicValue);
                }
                else if(!strcmp(topic, TOPIC_FADER2))
                {
                    fader2_topicValue = atoi(data);
                    printf("\tGet fader2 = %i\n", fader2_topicValue);
                }
                else if(!strcmp(topic, TOPIC_MOTOR_SPEED))
                {
                    motorSpeed_topicValue = atoi(data);
                    printf("\tGet motor speed = %i\n", motorSpeed_topicValue);
                }
                else if(!strcmp(topic, TOPIC_MOTOR_DIRECTION))
                {
                    motorDirection_topicValue = atoi(data);
                    printf("\tGet direction = %i\n", motorDirection_topicValue);
                }
                else
                {
                    printf("Error: Unknown topic:%s.\n", topic);
                }
            }


            break;
        case MQTT_EVENT_ERROR:
            printf("MQTT_EVENT_ERROR\n");
            break;
        default:
            printf("Other event id:%d\n", event->event_id);
            break;
    }
    return ESP_OK;
}

static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
        case SYSTEM_EVENT_STA_START:
            printf("SYSTEM_EVENT_STA_START\n");
            esp_wifi_connect();
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            printf("SYSTEM_EVENT_STA_GOT_IP\n");
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            printf("SYSTEM_EVENT_STA_DISCONNECTED: %u \n",event->event_info.disconnected.reason);
            esp_wifi_connect();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;
        case SYSTEM_EVENT_STA_CONNECTED:
            printf("ESP32 station connected to AP");
            break;
        default:
            printf("UNHANDLED EVENT! %d\n", event->event_id);
            break;
    }
    return ESP_OK;
}


void wifi_init()
{
    wifi_event_group = xEventGroupCreate();

    // printf("=== 1. Wi-Fi/LwIP Init Phase ===\n");

    // printf("s1.1: Create an LwIP core task and initialize LwIP-related work.\n");
    tcpip_adapter_init();

    //s1.2: Create a system Event task and initialize an application event’s callback function.
    //      In the scenario above, the application event’s callback function does nothing
    //      but relaying the event to the application task.
    // printf("s1.2: Create a system Event loop\n");
    //ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));

    //s1.3: Create the Wi-Fi driver task and initialize the Wi-Fi driver.
    // printf("s1.3: initialize the Wi-Fi driver\n");
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config;
    strcpy((char*)wifi_config.sta.ssid, WIFI_SSID);
    strcpy((char*)wifi_config.sta.password, WIFI_PASS);
    wifi_config.sta.scan_method = WIFI_FAST_SCAN;
    wifi_config.sta.bssid_set = 0;
    wifi_config.sta.channel = 0;
    wifi_config.sta.sort_method = WIFI_CONNECT_AP_BY_SIGNAL;
    wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
    wifi_config.sta.threshold.rssi = 0;

    wifi_config.sta.pmf_cfg.capable = true;
    wifi_config.sta.pmf_cfg.required = false;

    // printf("Connecting to %s...\n", wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());
    //ESP_ERROR_CHECK(esp_wifi_connect());

    // printf("Waiting for wifi\n");
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
    // s1.4: The main task calls OS API to create the application task.
}

int MotorSpeedPrev = 0;
int MotorDirectionPrev = 0;

int prevRaw1 = 0;
int prevRaw2 = 0;
int delta = 20;

char buff[10] = "";
int msg_id = 0;

int btn1_saved = 0;
int btn2_saved = 0;
int btn3_saved = 0;
uint32_t btn1PressTime = 0;
uint32_t btn2PressTime = 0;
uint32_t btnLongPress = 50000000;



esp_mqtt_client_handle_t client = nullptr;

void init_mqtt_client()
{
    esp_mqtt_client_config_t *mqtt_cfg = new esp_mqtt_client_config_t();
    mqtt_cfg->host = "192.168.1.149";
    mqtt_cfg->port = 1883;
    mqtt_cfg->username = "home";
    mqtt_cfg->password = "smart";
    mqtt_cfg->client_id = MQTT_CLIENT_ID;
    mqtt_cfg->event_handle = mqtt_event_handler;
    // .user_context = (void *)your_context

    client = esp_mqtt_client_init(mqtt_cfg);

    printf("DONE!\n");    
}

void mqtt_task_start(void *pvParameter)
{
    if(client == nullptr)
        printf("ERROR!!!!!");

    esp_mqtt_client_start(client);
    
    while(1)
    {
        if(abs(prevRaw1 - read_raw_1) > delta)
        {
            prevRaw1 = read_raw_1;
            memset(buff, 0, 10);
            __itoa(prevRaw1, buff, 10);
            printf("Send %s to %s\n", buff, TOPIC_FADER1);
            msg_id = esp_mqtt_client_publish(client, TOPIC_FADER1, buff, 0, 1, 1);
            // if(!msg_id)
            //     printf("Error: msg_id=%d Can't publish \"%s\" to \"%s\"!\n", msg_id, buff, TOPIC_FADER1);
            // else
            //     printf("Send ok. (msg_id=%d)", msg_id); 
        }

        
        if(abs(prevRaw2 - read_raw_2) > delta)
        {
            prevRaw2 = read_raw_2;
            memset(buff, 0, 10);
            __itoa(prevRaw2, buff, 10);
            printf("Send %s to %s\n", buff, TOPIC_FADER2);
            msg_id = esp_mqtt_client_publish(client, TOPIC_FADER2, buff, 0, 1, 1);
            // if(!msg_id)
            //     printf("Error: msg_id=%d Can't publish \"%s\" to \"%s\"!\n", msg_id, buff, TOPIC_FADER2);
            // else
            //     printf("Send ok. (msg_id=%d)", msg_id);
        }

        
                
        if(MotorSpeedPrev != MotorSpeed)
        {
            MotorSpeedPrev = MotorSpeed;
            memset(buff, 0, 10);
            __itoa(MotorSpeedPrev, buff, 10);
            printf("Send %s to %s\n", buff, TOPIC_MOTOR_SPEED);
            msg_id = esp_mqtt_client_publish(client, TOPIC_MOTOR_SPEED, buff, 0, 1, 1);
            // if(!msg_id)
            //     printf("Error: msg_id=%d Can't publish \"%s\" to \"%s\"!\n", msg_id, buff, TOPIC_MOTOR_SPEED);
            // else
            //     printf("Send ok. (msg_id=%d)\n", msg_id);
        }

        if(MotorDirectionPrev != MotorDirection)
        {
            MotorDirectionPrev = MotorDirection;
            memset(buff, 0, 10);
            __itoa(MotorDirectionPrev, buff, 10);
            printf("Send %s to %s\n", buff, TOPIC_MOTOR_DIRECTION);
            msg_id = esp_mqtt_client_publish(client, TOPIC_MOTOR_DIRECTION, buff, 0, 1, 1);
            // if(!msg_id)
            //     printf("Error: msg_id=%d Can't publish \"%s\" to \"%s\"!\n", msg_id, buff, TOPIC_MOTOR_DIRECTION);
            // else
            //     printf("Send ok. (msg_id=%d)\n", msg_id);
        }

        uint32_t currTicks = XTHAL_GET_CCOUNT();

#ifdef RC_BTN_PRESENT
        int b1 = gpio_get_level(KARNIZ_pin1);
        if (btn1_saved != b1)
        {
             btn1_saved = b1;
             printf("btn1=%d\n", btn1_saved);
             if (btn1_saved == 1)
             {
                btn1PressTime = currTicks;
                printf("-->>\n");

                // memset(buff, 0, 10);
                // __itoa(KARNIZ_RIGHT, buff, 10);
                // printf("Send %s to %s\n", buff, TOPIC_KARNIZ);
                buff[0] = KARNIZ_RIGHT;
                msg_id = esp_mqtt_client_publish(client, TOPIC_KARNIZ, buff, 0, 1, 1);
             }
             else
             {
                 printf("Press time:%d\n", currTicks - btn1PressTime);
                 if(currTicks - btn1PressTime > btnLongPress)
                 {
                     printf("STOP!\n");

                    //  memset(buff, 0, 10);
                    // __itoa(KARNIZ_STOP, buff, 10);
                    // printf("Send %s to %s\n", buff, TOPIC_KARNIZ);
                    buff[0] = KARNIZ_STOP;
                    msg_id = esp_mqtt_client_publish(client, TOPIC_KARNIZ, buff, 0, 1, 1);
                 }
             }
        }

        int b2 = gpio_get_level(KARNIZ_pin2);
        if (btn2_saved != b2)
        {
             btn2_saved = b2;
             printf("btn2=%d\n", btn2_saved);
             if (btn2_saved == 1)
             {
                btn2PressTime = currTicks;
                printf("<<--\n");

                // memset(buff, 0, 10);
                // __itoa(KARNIZ_RIGHT, buff, 10);
                // printf("Send %s to %s\n", buff, TOPIC_KARNIZ);
                buff[0] = KARNIZ_LEFT;
                msg_id = esp_mqtt_client_publish(client, TOPIC_KARNIZ, buff, 0, 1, 1);
             }
             else
             {
                 printf("Press time:%d\n", currTicks - btn2PressTime);
                 if(currTicks - btn2PressTime > btnLongPress)
                 {
                     printf("STOP!\n");

                    //  memset(buff, 0, 10);
                    // __itoa(KARNIZ_STOP, buff, 10);
                    // printf("Send %s to %s\n", buff, TOPIC_KARNIZ);
                    buff[0] = KARNIZ_STOP;
                    msg_id = esp_mqtt_client_publish(client, TOPIC_KARNIZ, buff, 0, 1, 1);
                 }
             }
        }

        int b3 = gpio_get_level(KARNIZ_pin3);
        if (btn3_saved != b3)
        {
             btn3_saved = b3;
             printf("btn3=%d\n", btn3_saved);
             if (btn3_saved == 1)
             {
                printf("STOP!\n");

                // memset(buff, 0, 10);
                // __itoa(KARNIZ_RIGHT, buff, 10);
                // printf("Send %s to %s\n", buff, TOPIC_KARNIZ);
                buff[0] = KARNIZ_STOP;
                msg_id = esp_mqtt_client_publish(client, TOPIC_KARNIZ, buff, 0, 1, 1);
             }
        }
#endif

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}


void WIFI_MQTT_task()
{
    printf("NVS init\n");
    nvs_flash_init();

    printf("wifi_init\n");
    wifi_init();

    printf("mqtt_task_init\n");
    init_mqtt_client();

    printf("mqtt_task_start\n");
    xTaskCreate(&mqtt_task_start, "WIFI_task", 8192, NULL, 5, NULL);
    //mqtt_task_start();
}
//===========================================================

