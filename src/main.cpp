// Andrey Datsko 2019

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
// #include "sdkconfig.h"

#include "global.hpp"
#include "blink_task.hpp"
#include "readADC_task.hpp"
#include "writeLEDC_task.hpp"
#include "motor_A4988_task.hpp"
#include "WIFI_MQTT_task.hpp"
#include "button.hpp"

#ifdef __cplusplus
extern "C" {
    void app_main(void);
}
#endif

void app_main()
{
    // TODO: Bluetooth wifi_stansby remote 
    // display ips
    // wifi_stansby:  seccond(reserved?) connect to RaspberryPI connect(dns?)
    xTaskCreate(&blink_task, "blink_task", 2048, NULL, 5, NULL);
    if(RUN_readADC_task)xTaskCreate(&readADC_task, "readADC_task", 2048, NULL, 5, NULL);
    xTaskCreate(&writeLED_task, "writeLED_task", 2048, NULL, 5, NULL);
    if(RUN_a4988_task)xTaskCreate(&a4988_task, "a4988_task", 4096, NULL, 5, NULL);
    
    WIFI_MQTT_task();

    init_btn_task();
    // xTaskCreate(&button_task, "button_task", 2048, NULL, 5, NULL);
}
