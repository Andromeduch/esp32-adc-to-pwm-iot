#include "global.hpp"

#include <driver/ledc.h>
#include <freertos/task.h>

int rawADCtoLEDC(int raw)
{
    // TODO:
    // NOTE: ln, not sin(   (x / 54800 - 0.0306) * (180/pi) ) * 4096 + 4096
    // duty max 8192
    // min < 150, 3180 < max 
    
    // !! NOT TESTED !!!

    /* AVOID LEDs blinking at disabled state:
     * death zone,
     * to be sure that the knob has reached
     * the end of the slider range 
     * and the min/max values have been reached
     */

    // TODO:
    // 1. At start
    // Do not send topic value from faders(initVal=0), but get from broker
    // 

// const int LED_blindSpot = 0;//40;
// const int ADC_minVal = 115 + LED_blindSpot;
// const int ADC_maxVal = 3190 - ADC_minVal - LED_blindSpot; // = 3075
// const int PWM_fullDuty = 8192;

    return ( (float)PWM_fullDuty / (float)ADC_maxVal * (float)raw );
    



    int duty = raw;

    if(duty < 0){
        printf("Wrong convert duty = %i\n", duty);
        duty = 0;
    }
    
    int rawRange = ADC_maxVal - ADC_minVal; // ~3k
    int slowRange = rawRange / 3; // ~1k
    float fastK = (PWM_fullDuty - slowRange) / (float)(rawRange - slowRange) ; // (8k-1k) / (3k-1k) = 7k / 2k = 3,5   
    if(duty > slowRange)
    {
        // printf("%d + (%d-%d) * %d")
        duty = slowRange +  // 1k
            (duty - slowRange) *  fastK; // 3k-1k * 3,5 = 1k + 2k*3,5 = 1k + 7 = 8k

        if (duty > PWM_fullDuty)
            duty = PWM_fullDuty;
    }

    //printf("%d\t-> %d\n", raw, duty);

    return duty;
}

#define LEDC_HS_TIMER          LEDC_TIMER_0
#define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
#define LEDC_HS_CH0_GPIO       (32) // GPIO NUM 32
#define LEDC_HS_CH0_CHANNEL    LEDC_CHANNEL_0
#define LEDC_HS_CH1_CHANNEL    LEDC_CHANNEL_1

#define LEDC_TEST_CH_NUM       (2)
#define LEDC_TEST_DUTY         (4000)
#define LEDC_TEST_FADE_TIME    (3000)
void writeLED_task(void *pvParameter)
{
    // frequency of 5 kHz can have the maximum duty resolution of 13 bits.(8192)
    /*
 * About this example
 *
 * 1. Start with initializing LEDC module:
 *    a. Set the timer of LEDC first, this determines the frequency
 *       and resolution of PWM.
 *    b. Then set the LEDC channel you want to use,
 *       and bind with one of the timers.
 *
 * 2. You need first to install a default fade function,
 *    then you can use fade APIs.
 *
 * 3. You can also set a target duty directly without fading.
 *
 * 4. This example uses GPIO18/19/4/5 as LEDC output,
 *    and it will change the duty repeatedly.
 *
 * 5. GPIO18/19 are from high speed channel group.
 *    GPIO4/5 are from low speed channel group.
 *
 */

    int ch;

    /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
    ledc_timer_config_t ledc_timer;
    ledc_timer.duty_resolution = LEDC_TIMER_13_BIT; // resolution of PWM duty
    ledc_timer.freq_hz = 5000;                      // frequency of PWM signal
    ledc_timer.speed_mode = LEDC_HS_MODE;           // timer mode
    ledc_timer.timer_num = LEDC_HS_TIMER;            // timer index
    
    // Set configuration of timer0 for high speed channels
    ledc_timer_config(&ledc_timer);

    // Prepare and set configuration of timer1 for low speed channels
    // ledc_timer.speed_mode = LEDC_LS_MODE;
    // ledc_timer.timer_num = LEDC_LS_TIMER;
    // ledc_timer_config(&ledc_timer);

    /*
     * Prepare individual configuration
     * for each channel of LED Controller
     * by selecting:
     * - controller's channel number
     * - output duty cycle, set initially to 0
     * - GPIO number where LED is connected to
     * - speed mode, either high or low
     * - timer servicing selected channel
     *   Note: if different channels use one timer,
     *         then frequency and bit_num of these channels
     *         will be the same
     */
    ledc_channel_config_t ledc_channel[LEDC_TEST_CH_NUM];

    ledc_channel[0].channel    = LEDC_HS_CH0_CHANNEL;
    ledc_channel[0].duty       = 0;
    ledc_channel[0].gpio_num   = LEDC_HS_CH0_GPIO;
    ledc_channel[0].speed_mode = LEDC_HS_MODE;
    ledc_channel[0].hpoint     = 0;
    ledc_channel[0].timer_sel  = LEDC_HS_TIMER;

    ledc_channel[1].channel    = LEDC_HS_CH1_CHANNEL;
    ledc_channel[1].duty       = 0;
    ledc_channel[1].gpio_num   = LEDC_HS_CH1_GPIO;
    ledc_channel[1].speed_mode = LEDC_HS_MODE;
    ledc_channel[1].hpoint     = 0;
    ledc_channel[1].timer_sel  = LEDC_HS_TIMER;

    // Set LED Controller with previously prepared configuration
    for (ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
        ledc_channel_config(&ledc_channel[ch]);
    }

    // Initialize fade service.
    ledc_fade_func_install(0);


    //88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888

    while(1)
    { 
        //printf("write");


        // NOTE: standalone mode автономный режим


        static int prev_fader1_topicValue = -1;
        static int prev_fader2_topicValue = -1;

        motorDirection_topicValue;
        motorSpeed_topicValue;

        
        static int prevRaw1 = 0;
        static int prevRaw2 = 0;
        int delta = 10;//3;

        auto set_LED_Duty = [&ledc_channel](size_t ch, int rawADCvalue){
                if (ch != 0 && ch != 1) {  printf("ERROR: wrong chanel"); return; }
                int duty = rawADCtoLEDC(rawADCvalue);
                ledc_set_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel, duty);
                ledc_update_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel);
            };

        if (prev_fader1_topicValue != fader1_topicValue)
        {
            prev_fader1_topicValue = fader1_topicValue;
            set_LED_Duty(0, fader1_topicValue);
        }
        else
            if(abs(prevRaw1 - read_raw_1) > delta)
        {
            prevRaw1 = read_raw_1;
            set_LED_Duty(0, read_raw_1);
        }
        

        if (prev_fader2_topicValue != fader2_topicValue)
        {
            prev_fader2_topicValue = fader2_topicValue;
            set_LED_Duty(1, fader2_topicValue);
        }
        else
            if(abs(prevRaw2 - read_raw_2) > delta)
        {
            prevRaw2 = read_raw_2;
            set_LED_Duty(1, read_raw_2);
        }    

        vTaskDelay(50 / portTICK_PERIOD_MS);

        // printf("1. LEDC fade up to duty = %d\n", LEDC_TEST_DUTY);
        // for (ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
        //     ledc_set_fade_with_time(ledc_channel[ch].speed_mode,
        //             ledc_channel[ch].channel, LEDC_TEST_DUTY, LEDC_TEST_FADE_TIME);
        //     ledc_fade_start(ledc_channel[ch].speed_mode,
        //             ledc_channel[ch].channel, LEDC_FADE_NO_WAIT);
        // }
        // vTaskDelay(LEDC_TEST_FADE_TIME / portTICK_PERIOD_MS);

        // printf("2. LEDC fade down to duty = 0\n");
        // for (ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
        //     ledc_set_fade_with_time(ledc_channel[ch].speed_mode,
        //             ledc_channel[ch].channel, 0, LEDC_TEST_FADE_TIME);
        //     ledc_fade_start(ledc_channel[ch].speed_mode,
        //             ledc_channel[ch].channel, LEDC_FADE_NO_WAIT);
        // }
        // vTaskDelay(LEDC_TEST_FADE_TIME / portTICK_PERIOD_MS);

        // printf("3. LEDC set duty = %d without fade\n", LEDC_TEST_DUTY);
        // for (ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
        //     ledc_set_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel, LEDC_TEST_DUTY);
        //     ledc_update_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel);
        // }
        // vTaskDelay(1000 / portTICK_PERIOD_MS);

        // printf("4. LEDC set duty = 0 without fade\n");
        // for (ch = 0; ch < LEDC_TEST_CH_NUM; ch++) {
        //     ledc_set_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel, 0);
        //     ledc_update_duty(ledc_channel[ch].speed_mode, ledc_channel[ch].channel);
        // }
        // vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

