#include "global.hpp"
#include "iot_a4988.h"
#include <cmath>

// ========= A4988 ======================================
//#include "https://github.com/espressif/esp-iot-solution/blob/master/components/motor/stepper/a4988/include/iot_a4988.h"

volatile int MotorSpeed = 0;
volatile int MotorDirection = 0;
void a4988_task(void *pvParameter)
{
    /**
     * @brief Constructor for CA4988Stepper class
     * @param step_io Output GPIO for step signal
     * @param dir_io Output GPIO for direction signal
     * @param number_of_steps The number of steps in one revolution of the stepper motor.
     * @param speed_mode LEDC mode
     * @param tim_idx LEDC time index
     * @param chn LEDC channel index
     * @param pcnt_unit PCNT unit index
     * @param pcnt_chn PCNT channel index
     *
     * @note Different stepper object must use different LEDC CHANNELS and PCNT UNITS.
     *       Make sure the LEDC CHANNELS and PCNT UNITS you are using do not conflict with other modules.
     */

    int step_io = STEP_IO;
    int dir_io = DIR_IO;
    int number_of_steps = 200 ;//* 16
    ledc_mode_t speed_mode = LEDC_HIGH_SPEED_MODE;
    ledc_timer_t tim_idx = LEDC_TIMER_1;
    ledc_channel_t chn = LEDC_CHANNEL_2;
    pcnt_unit_t pcnt_unit = PCNT_UNIT_0;
    pcnt_channel_t pcnt_chn = PCNT_CHANNEL_0;

    CA4988Stepper stepper(step_io, dir_io, number_of_steps, speed_mode, tim_idx, chn, pcnt_unit, pcnt_chn);
    int speed = 2800;
    stepper.setSpeedRpm(speed);
    stepper.run(1, false);


    


    while(1){

        static int prevRaw1 = 0;
        static int prevRaw2 = 0;

        static int motorDirection_topicValue_prew = 0;
        static int motorSpeed_topicValue_prew = 0;

        int delta = 15;

        int medium = ADC_maxVal/2;

        if(motorDirection_topicValue_prew != motorDirection_topicValue ||
            motorSpeed_topicValue_prew != motorSpeed_topicValue)
        {
            motorDirection_topicValue_prew = motorDirection_topicValue;
            motorSpeed_topicValue_prew = motorSpeed_topicValue;

            printf("Set speed from topic: %d, dir: %d\n", motorSpeed_topicValue_prew,  motorDirection_topicValue_prew);
            stepper.setSpeedRpm(motorSpeed_topicValue_prew);
            stepper.run(motorDirection_topicValue_prew, true);
        }
        else
         if(abs(prevRaw1 - read_raw_1) > delta
         ||abs(prevRaw2 - read_raw_2) > delta )
        {
            if(abs(prevRaw1 - read_raw_1) > delta)
                prevRaw1 = read_raw_1;
            if(abs(prevRaw2 - read_raw_2) > delta)
                prevRaw2 = read_raw_2;
            
            float speed =  abs(prevRaw1 - medium);
            float multiplier = (float)prevRaw2 / ((float)ADC_maxVal * 4 );//medium; 

            MotorSpeed = std::ceil(speed*multiplier);

            stepper.setSpeedRpm(MotorSpeed);
            MotorDirection = prevRaw1 < medium;
            printf("Set speed: %d (%.2f * %.2f), dir: %d\n", MotorSpeed, speed, multiplier,  MotorDirection);
            stepper.run(MotorDirection, true);
        }

        // TODO: move by slider
        // if(abs(prevRaw2 - read_raw_2) > delta)
        // {
        //     prevRaw2 = read_raw_2;
        //     int duty_2 = rawADCtoLEDC(prevRaw2);
        //     ledc_set_duty(ledc_channel[1].speed_mode, ledc_channel[1].channel, duty_2);
        //     ledc_update_duty(ledc_channel[1].speed_mode, ledc_channel[1].channel);     
        // }  

        //stepper.step(10);
        
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}