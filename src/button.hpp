// карниз 3 копки:
//  "<-"(долгое/короткое нажатие = двигать влево/закрыть = manual/auto mode), stop, ->. 

#include <global.hpp>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
// #include "freertos/queue.h"
#include "driver/gpio.h"

#define GPIO_INPUT_PIN_SEL  ((1ULL<<KARNIZ_pin1) | (1ULL<<KARNIZ_pin2) | (1ULL<<KARNIZ_pin3))
#define ESP_INTR_FLAG_DEFAULT 0

// static xQueueHandle gpio_evt_queue = NULL;

// static void IRAM_ATTR gpio_isr_handler(void* arg)
// {
//     uint32_t gpio_num = (uint32_t) arg;
//     xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
// }

// void init_btn_task(void);

// static void button_task(void* arg)
// {
//     init_btn_task();

//     uint32_t io_num;
//     for(;;) {
//         if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
//             printf("GPIO[%d] intr, val: %d\n", io_num, gpio_get_level((gpio_num_t)io_num));
//         }
//         vTaskDelay(50 / portTICK_PERIOD_MS);
//     }
// }

void init_btn_task(void)
{
    gpio_config_t io_conf;

    // printf("interrupt of rising edge\n");
    io_conf.intr_type = GPIO_INTR_DISABLE;//POSEDGE;
    
    // printf("bit mask of the pins, use GPIO 2, 4, 15 here\n");
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    
    // printf("set as input mode\n");
    io_conf.mode = GPIO_MODE_INPUT;
    
    // printf("enable pull-down mode\n");
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    
    // printf("disable pull-up mode\n");
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    
    gpio_config(&io_conf);

    
    // printf("change gpio intrrupt type for one pin\n");
    // gpio_set_intr_type(KARNIZ_pin1, GPIO_INTR_ANYEDGE);
    // gpio_set_intr_type(KARNIZ_pin2, GPIO_INTR_ANYEDGE);
    // gpio_set_intr_type(KARNIZ_pin3, GPIO_INTR_ANYEDGE);

    
    // printf("create a queue to handle gpio event from isr\n");
    // gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    

    
    // printf("install gpio isr service\n");
    // gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    
    // printf("hook isr handler for specific gpio pin\n");
    // gpio_isr_handler_add(KARNIZ_pin1, gpio_isr_handler, (void*) KARNIZ_pin1);
    // gpio_isr_handler_add(KARNIZ_pin2, gpio_isr_handler, (void*) KARNIZ_pin2);
    // gpio_isr_handler_add(KARNIZ_pin3, gpio_isr_handler, (void*) KARNIZ_pin3);
    
    
    // printf("remove isr handler for gpio number.\n");
    // gpio_isr_handler_remove(KARNIZ_pin1);
    // gpio_isr_handler_remove(KARNIZ_pin2);
    // gpio_isr_handler_remove(KARNIZ_pin3);
    
    // printf("hook isr handler for specific gpio pin again\n");
    // gpio_isr_handler_add(KARNIZ_pin1, gpio_isr_handler, (void*) KARNIZ_pin1);
    // gpio_isr_handler_add(KARNIZ_pin2, gpio_isr_handler, (void*) KARNIZ_pin2);
    // gpio_isr_handler_add(KARNIZ_pin3, gpio_isr_handler, (void*) KARNIZ_pin3);
}