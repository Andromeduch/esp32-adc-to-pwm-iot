#include "global.hpp"

#include <algorithm>
#include <esp_adc_cal.h>
#include <freertos/task.h>

volatile int read_raw_1 = 0;
volatile int read_raw_2 = 0;

static void check_efuse(void)
{
    //Check TP is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP) == ESP_OK) {
        printf("eFuse Two Point: Supported\n");
    } else {
        printf("eFuse Two Point: NOT supported\n");
    }

    //Check Vref is burned into eFuse
    if (esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF) == ESP_OK) {
        printf("eFuse Vref: Supported\n");
    } else {
        printf("eFuse Vref: NOT supported\n");
    }
}
static void print_char_val_type(esp_adc_cal_value_t val_type)
{
    if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP) {
        printf("Characterized using Two Point Value\n");
    } else if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
        printf("Characterized using eFuse Vref\n");
    } else {
        printf("Characterized using Default Vref\n");
    }
}



void readADC_task(void *pvParameter)
{

    /*
    NOTE
    ADC min 0.2V, max 3.1V(3.3-0.2)
    ADDED 2 resistors of 1kOm near 10kOm (1k-10k-1k)
    vRef 3.3V
    max1 3188 2610mV
    max2 3190 2610mv
    min1 113 235mV
    min2 118 230mV

    // min < 128, 3180 < max
    */


    //Check if Two Point or Vref are burned into eFuse
    check_efuse();

    adc_atten_t attenuation = ADC_ATTEN_DB_11;//ADC_ATTEN_DB_0;//ADC_ATTEN_DB_11
    adc_bits_width_t adc_width = ADC_WIDTH_BIT_12;

    adc1_config_width(adc_width);
    adc1_config_channel_atten(ADC1_CHANNEL_7, attenuation);
    adc1_config_channel_atten(ADC1_CHANNEL_6, attenuation);

    //Characterize ADC
    static esp_adc_cal_characteristics_t *adc_chars;
    adc_chars = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
    #define DEFAULT_VREF    3300 // not 3600 ?
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(
        ADC_UNIT_1, attenuation, adc_width, DEFAULT_VREF, adc_chars);
    print_char_val_type(val_type);

    while(1)
    { 

        // read_raw_1 = adc1_get_raw( ADC1_CHANNEL_6);
        // read_raw_2 = adc1_get_raw( ADC1_CHANNEL_7);

        int count = NUMBER_OF_SEMPLS;
        int tmp1= 0;
        int tmp2 = 0;

        for(int i = 0; i < count; i++){
            tmp1 += adc1_get_raw( ADC1_CHANNEL_6);
            tmp2 += adc1_get_raw( ADC1_CHANNEL_7);
                        
            //printf("%d\t%d\n", tmp1, tmp2);

        }
        read_raw_1 = std::min( std::max(0, tmp1/count - ADC_minVal), ADC_maxVal );
        read_raw_2 = std::min( std::max(0, tmp2/count - ADC_minVal), ADC_maxVal );
        //printf("===== %d\t%d ====\n", read_raw_1, read_raw_2);

        //printf("==================================================================================================\n");

        //uint32_t voltage1 = esp_adc_cal_raw_to_voltage(read_raw_1, adc_chars);
        //printf("1: %d\t%dmV\t", read_raw_1, voltage1);
        // for(int i = 0; i < read_raw_1; i+=50)
        //     printf("#");
        //printf("\n");

        //uint32_t voltage2 = esp_adc_cal_raw_to_voltage(read_raw_2, adc_chars);
        //printf("2: %d\t%dmV\t", read_raw_2, voltage2);
        // for(int i = 0; i < read_raw_2; i+=50)
        //     printf("#");
        //printf("\n");

        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
}